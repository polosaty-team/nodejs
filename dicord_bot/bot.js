
var Discord = require('discord.io');
// const TelegramBot = require('node-telegram-bot-api');
// var logger = require('winston');
var auth = require('./auth.json');
// Configure logger settings

const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;
const myFormat = printf(info => {
  return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
});
const logger = createLogger({
 format: combine(
    format.colorize(),
    label({ label: 'discord_bot' }),
    timestamp(),
    // format.json(),
    myFormat
  ),
  transports: [new transports.Console({
    // colorize: true,
    // format: format.simple()
  })]
});

const isEmptyObj = (o) => !(Object.keys(o).length);

// // Initialize Telegram Bot
// const telegram_bot = new TelegramBot(token, {
//   polling: true,
//   request: {
//     proxy: 'http://proxy.antizapret.prostovpn.org:3128',
//     // proxy: 'HTTPS://proxy.antizapret.prostovpn.org:3143',
//     // proxy: 'http://87.140.92.161:56849'
//   }
// });


// telegram_bot.on('message', (msg) => {
//   const chatId = msg.chat.id;

//   // send a message to the chat acknowledging receipt of their message
//   let jsonMsg = JSON.stringify(msg);
//   logger.info(`${chatId}: Received message ${jsonMsg}`)
//   // bot.sendMessage(chatId, `Received your message ${jsonMsg} in ${chatId}`);
// });

// Initialize Discord Bot
var diccord_bot = new Discord.Client({
   token: auth.discord.token,
   autorun: true
});



diccord_bot.on('ready', function (evt) {
    console.log('ready');
    logger.info('Connected');
    logger.info('Logged in as: ');
    logger.info(diccord_bot.username + ' - (' + diccord_bot.id + ')');
});
// https://discord.js.org/#/docs/main/stable/class/Client?scrollTo=e-guildMemberAvailable
diccord_bot.on('guildMemberAvailable', function (member) {
    logger.info('guildMemberAvailable ' + JSON.stringify(member));
});

// https://discord.js.org/#/docs/main/stable/class/Client?scrollTo=e-voiceStateUpdate
diccord_bot.on('voiceStateUpdate', function (evt) {
    // logger.info('voiceStateUpdate evt:' + JSON.stringify(evt) );
    // evt.s
    

    logger.info('voiceStateUpdate evt:' + `${ evt.s } ${evt.d.member.user.username}` );
    /*
    {
      "t": "VOICE_STATE_UPDATE",
      "s": 3,
      "op": 0,
      "d": {
        "member": {
          "user": {
            "username": "polosaty_test",
            "id": "503526329807929345",
            "discriminator": "0116",
            "avatar": null
          },
          "roles": [],
          "nick": null,
          "mute": false,
          "joined_at": "2018-10-21T11:15:50.156395+00:00",
          "deaf": false
        },
        "user_id": "503526329807929345",
        "suppress": false,
        "session_id": "3fbef2c24f39bef8b0189c72d67edd52",
        "self_video": false,
        "self_mute": true,
        "self_deaf": false,
        "mute": false,
        "guild_id": "285679989192392704",
        "deaf": false,
        "channel_id": "285680063561596929"
      }
    }*/
    debugger;
    // let channel = diccord_bot.channels.get(evt.d.channel_id);
    // let e_data = evt.d;
    let channel = diccord_bot.channels[evt.d.channel_id]  // .name
    let user = evt.d.member.user //.username
    for(c_id in diccord_bot.channels){
        
        let c_name = diccord_bot.channels[c_id].name;
        if (! isEmptyObj(diccord_bot.channels[c_id].members)){
            let members_names = Object.keys(diccord_bot.channels[c_id].members).map(user_id => diccord_bot.users[user_id].username);
            logger.info(`Channel: ${c_name}: ${members_names.join(', ')}`)
        }
    }

    // logger.info('voiceStateUpdate channel: ' + JSON.stringify(channel));
});

diccord_bot.on('message', function (user, userID, channelID, message, evt) {
    logger.info('Message :' + message);
    // Our diccord_bot needs to know if it will execute a command
    // It will listen for messages that will start with `!`
    if (message.substring(0, 1) == '!') {
        var args = message.substring(1).split(' ');
        var cmd = args[0];

        args = args.splice(1);
        switch(cmd) {
            // !ping
            case 'ping':
                diccord_bot.sendMessage({
                    to: channelID,
                    message: 'Pong!'
                });

            break;
            // Just add any case commands if you want to..
         }
     }
});

console.log('fin');

